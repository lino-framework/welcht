# -*- coding: UTF-8 -*-
# Copyright 2002-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

import sys

PY2 = sys.version_info[0] == 2

requires = [
    'lino-welfare', 'pytidylib', 'django-iban', 'metafone', 'cairocffi'
]
if PY2:
    requires.append('channels<2')
    requires.append('suds')
else:
    requires.append('channels')
    requires.append('suds-py3')
    # requires.append('suds-jurko')

SETUP_INFO = dict(
    name='lino-welcht',
    version='24.5.0',
    install_requires=requires,
    test_suite='tests',
    tests_require=['pytest'],
    include_package_data=True,
    zip_safe=False,
    description="A Lino Django application for the PCSW of Châtelet",
    long_description=u"""\
**Lino Welfare Châtelet** is a
`Lino Welfare <https://welfare.lino-framework.org>`__
application developed and maintained for the PCSW of Châtelet in Belgium.

- The central project homepage is http://welcht.lino-framework.org

- For *introductions* and *commercial information*
  see `www.saffre-rumma.net
  <https://www.saffre-rumma.net/welfare/>`__.

- Technical specifications and test suites are in
  `Lino Welfare <https://welfare.lino-framework.org>`__.

- The `French project homepage <https://fr.welfare.lino-framework.org>`__
  contains release notes and end-user docs.

- Online demo site at https://welfare-demo.lino-framework.org

""",
    author='Rumma & Ko Ltd',
    author_email='hamza@lino-framework.org',
    url="https://gitlab.com/lino-framework/welcht",
    license_files=['COPYING'],
    classifiers="""\
Programming Language :: Python
Programming Language :: Python :: 3
Development Status :: 5 - Production/Stable
Environment :: Web Environment
Framework :: Django
Intended Audience :: Developers
Intended Audience :: System Administrators
License :: OSI Approved :: GNU Affero General Public License v3
Natural Language :: English
Natural Language :: French
Natural Language :: German
Operating System :: OS Independent
Topic :: Database :: Front-Ends
Topic :: Home Automation
Topic :: Office/Business
Topic :: Sociology :: Genealogy
Topic :: Education""".splitlines())

SETUP_INFO.update(packages=[
    'lino_welcht', 'lino_welcht.lib', 'lino_welcht.lib.courses',
    'lino_welcht.lib.courses.fixtures', 'lino_welcht.lib.cv',
    'lino_welcht.lib.cv.fixtures', 'lino_welcht.lib.isip',
    'lino_welcht.lib.pcsw', 'lino_welcht.lib.pcsw.fixtures'
])

SETUP_INFO.update(include_package_data=True)
