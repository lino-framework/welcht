# -*- coding: UTF-8 -*-
# Copyright 2014-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""Contains plugins specific to :ref:`welcht`.

.. autosummary::
   :toctree:

   cv
   courses
   isip
   pcsw

"""
