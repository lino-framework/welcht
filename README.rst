===========================
The ``lino-welcht`` package
===========================



**Lino Welfare Châtelet** is a
`Lino Welfare <https://welfare.lino-framework.org>`__
application developed and maintained for the PCSW of Châtelet in Belgium.

- The central project homepage is http://welcht.lino-framework.org

- For *introductions* and *commercial information*
  see `www.saffre-rumma.net
  <https://www.saffre-rumma.net/welfare/>`__.

- Technical specifications and test suites are in
  `Lino Welfare <https://welfare.lino-framework.org>`__.

- The `French project homepage <https://fr.welfare.lino-framework.org>`__
  contains release notes and end-user docs.

- Online demo site at https://welfare-demo.lino-framework.org


